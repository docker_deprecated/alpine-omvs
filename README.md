# alpine-omvs

#### [alpine-x64-omvs](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-omvs/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64build/alpine-x64-omvs.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-omvs "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64build/alpine-x64-omvs.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-omvs "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-omvs.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-omvs/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-omvs.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-omvs/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Appplication : [Oh! Multicast Video Scanner](http://ohhara.sarang.net/omvs/)
    - omvs scans all multicast ip addresses the user specifies and if omvs finds something, omvs tries to decode it with gstreamer and saves it as png file in the output directory
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -i \
           -t \
           --rm \
           --privileged \
           --net=host \
           forumi0721alpinex64build/alpine-x64-omvs:latest
```


----------------------------------------
#### Usage

```
omvs
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| --privileged       | Give extended privileges to this container       |
| --net=host         | Connect a container to host network              |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /omvs_out          | omvs output                                      |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |



----------------------------------------
* [forumi0721alpinex64build/alpine-x64-omvs](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-omvs/)

